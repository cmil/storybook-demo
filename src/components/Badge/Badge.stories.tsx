import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import Badge from './Badge';

export default {
  title: 'Simple Components/Badge',
  component: Badge,
  parameters: {
    componentSubtitle: 'A badge is a badge is a badge'
  },
  decorators: [withKnobs]
};

export const Default = () => {
  const label = text('Label', 'Foo');
  const compact = boolean('Compact', false);
  const dimmed = boolean('Dimmed', false);
  return (
    <Badge
      label={label}
      compact={compact}
      dimmed={dimmed}
      onClick={action('click clack')}
    />
  );
};

export const Dimmed = () => <Badge label="Foo" dimmed />;
// Unfortunately we cannot use doc comments for adding a story description.
// There is an open issue requesting that feature:
// https://github.com/storybookjs/storybook/issues/8527
Dimmed.story = {
  parameters: {
    docs: {
      storyDescription: 'A `Badge` can be dimmed to decrease highlighting intensity.'
    }
  }
};

const CompactDesc = `
A compact \`Badge\` may not be so compact after all.

So what is it good for?`;

export const Compact = () => <Badge label="Compact" compact />;
Compact.story = { parameters: { docs: { storyDescription: CompactDesc } } };

const BlueDesc = `\`Badge\`s may be blue.`;

export const Blue = () => <Badge label="Foo" blue />;
Blue.story = { parameters: { docs: { storyDescription: BlueDesc } } };

const OrangeDesc = `\`Badge\`s may be orange as well.`;

export const Orange = () => <Badge label="Foo" orange />;
Orange.story = { parameters: { docs: { storyDescription: OrangeDesc } } };
