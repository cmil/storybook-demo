import React from 'react';
import renderer from 'react-test-renderer';
import { Blue, Dimmed, Orange } from './Badge.stories';

it('blue edited label renders correctly', () => {
  const tree = renderer.create(Blue());
  expect(tree.toJSON()).toMatchSnapshot();
});

it('renders dimmed', () => {
  const tree = renderer.create(Dimmed());
  expect(tree.toJSON()).toMatchSnapshot();
});

it('renders orange', () => {
  const tree = renderer.create(Orange());
  expect(tree.toJSON()).toMatchSnapshot();
});
