import * as React from 'react';
import classnames from 'classnames/bind';
import style from './Badge.module.scss';

interface Props {
  label: string
  compact?: boolean
  dimmed?: boolean
  orange?: boolean
  blue?: boolean
  onClick?: Function
}

/**
 * Use `Badge` to highlight key info with a predefined status.
 */
const Badge = (props: Props) => {
  const { label, compact, dimmed, orange, blue, onClick } = props;
  const cx = classnames.bind(style);
  const badgeClass = cx('badge', {
    'bagde--compact': compact,
    'badge--dimmed': dimmed,
    'badge--orange': orange,
    'badge--blue': blue,
  });

  function handleClick (event: any) {
    if (onClick) onClick(event);
  }

  return <span className={badgeClass} onClick={handleClick}>{label}</span>;
};

export default Badge;
