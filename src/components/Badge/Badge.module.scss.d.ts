export const badge: string;
export const badgeCompact: string;
export const badgeDimmed: string;
export const badgeOrange: string;
export const badgeBlue: string;
