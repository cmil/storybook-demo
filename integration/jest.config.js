module.exports = {
  preset: 'jest-puppeteer',
  testRegex: './*\\.test\\.[jt]s$',
  setupFilesAfterEnv: ['./setupTests.ts']
};
