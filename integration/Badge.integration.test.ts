it('visually renders orange', async () => {
  // APIs from jest-puppeteer
  await page.goto('http://localhost:9099/iframe.html?id=simple-components-badge--orange');
  const image = await page.screenshot();
  
  // API from jest-image-snapshot
  expect(image).toMatchImageSnapshot();
});
